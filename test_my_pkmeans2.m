close all; clearvars; clc
sigma = 0;
x = -5:0.1:5; x = x';
y1 = - 3 * x + 5 + randn(length(x), 1) * sigma;
y2 = 4 * x - 5 + randn(length(x), 1) * sigma;
y3 = x + 8 + randn(length(x), 1) * sigma;
fig = figure(1)
plot(x, y1, 'bo', 'MarkerSize', 1, 'MarkerFaceColor', 'b')
hold on 
plot(x, y2, 'bo', 'MarkerSize', 1, 'MarkerFaceColor', 'b')
plot(x, y3, 'bo', 'MarkerSize', 1, 'MarkerFaceColor', 'b')
hold off
grid on
xlabel('x')
ylabel('y')
legend('- 3x + 5', ...
       ' 4x -  5', ...
       ' x + 8')
grid off
legend off
saveas(fig, 'dat_image.png')
data = [x y1; x y2; x y3];
%% hirarchial clustering
figure(2)
Y = data';
Z = linkage(Y(1:2, :)','single','euclidean','savememory','on');
c = cluster(Z, 'maxclust', 3);
scatter(Y(1,:), Y(2,:), 10, c);
%% RUN MY ALGO
best_sum = zeros(5,1);
best_k = 0;
for k=1:5
    clust = mypolykmean(data(:,2), data(:,1), k, 1);
    sum = 0;
    for c=1:k
        sum = sum + clust{c}.R;
    end
    best_sum(k) = sum;
end
best_sum


%% compare with Hough transform
im = zeros(201, 201);
data2 = data - repmat([min(data(:,1)), min(data(:,2))], size(data,1), 1);
data2 = 200 * ([data2(:,1) / max(data2(:, 1)), data2(:,2) / max(data2(:, 2))]) + 1;
data2 = floor(data2);
% im(data2) = 0;
for i=1:size(data2, 1)
    im(data2(i,2), data2(i,1)) = 1;
end
sum(sum(im))
imshow(im)
hold on


[H,T,R] = hough(im,'RhoResolution',0.1,'ThetaResolution',0.01);

P  = houghpeaks(H,5,'threshold',ceil(0.3*max(H(:))));
lines = houghlines(im,T,R,P,'FillGap',500,'MinLength',7);
max_len = 0;
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');

   % Plot beginnings and ends of lines
   plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
   plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');

   % Determine the endpoints of the longest line segment
   len = norm(lines(k).point1 - lines(k).point2);
   if ( len > max_len)
      max_len = len;
      xy_long = xy;
   end
end
hold off

%% new algo 
N = size(data,1);
poss_lines = zeros(N * (N-1) / 2, 2);
k = 1;
for i=1:N
    for j=(i+1):N
        x1 = data(i,1); y1 = data(i,2); x2 = data(j, 1); y2 = data(j, 2);
        a = (y2 - y1) / (x2 - x1);
        b = y2 - a * x2;
        poss_lines(k,1) = a;
        poss_lines(k,2) = b;
        k = k + 1;
    end
end
plot(poss_lines(:,1), poss_lines(:,2), '.')
lines = kmeans(poss_lines, 3)