classdef my_hough_transform
    %MY_HOUGH_TRANSFORM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        theta
        matrix
        rho
    end
    
    methods
        function ht = my_hough_transform(ht)
            ht.theta = -90:0.5:90; ht.theta = ht.theta / 360 * 2 * pi;
            ht.rho = -10:0.01:10;
            ht.matrix = zeros(length(ht.rho), length(ht.theta));
        end
        
        function rhoq = quantize_rho(ht, x)
            xMin = 0.1;
            xMax = ht.rho(end) - ht.rho(1)+0.1;
            N = length(ht.rho);
            rhoq = floor(N * ((x-ht.rho(1)+0.1) / xMin) /(xMax/xMin));
        end
        
        function plot_matrix(ht)
            figure(41)
            image(ht.matrix)
            xlabel('\theta')
            ylabel('\rho')
            
        end
        
        function r = activation_function(ht, x, y, theta)
            r = cos(theta) * x + sin(theta) * y;
        end
        
        function idx = find_index_of_rho(ht, r)
            rhos = repmat(ht.rho, 1, length(r)) - r;
            idx = zeros(length(r), 1);
            for i=1:length(r)
                curr_col = rhos(:,i);
                try
                    idx(i) = find(curr_col > 0, 1);
                catch
                    idx(i) = length(curr_col);
                end                    
            end
        end
        
        function ht = upvote(ht, idx, t)
            neighbor = 1;
            for i=1:length(idx)            
                for j=-neighbor:neighbor
                    try
                        ht.matrix(idx(i)+j, t) = ht.matrix(idx(i)+j, t) + 1;
                    catch
                    end
                end
            end
        end
        
        function ht = update_matrix(ht, x, y)
            for t = 1:length(ht.theta)
                tet = ht.theta(t);
                r = ht.activation_function(x, y, tet);
                idx = ht.quantize_rho(r);
                ht = ht.upvote(idx, t);
                
            end 
        end
    end
    
end

